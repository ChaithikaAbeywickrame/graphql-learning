
/**
 * If an value is added to be filtered 
 * It will be assigned to where or else it would be just empty
 */
async function feed (parent, args, context, info) {
    const where = args.filter ? {
        OR: [
            { description_contains: args.filter },
            { url_contains: args.filter },
        ],
    } : {}

    const links = await context.prisma.links({
        where,
        skip: args.skip,
        first: args.first,
        orderBy: args.orderBy
    })

    const count = await context.prisma.linksConnection({
        where,
    })
    .aggregate()
    .count()


    return {
        links,
        count,
    }
}

// info: () => `this is the sample information`,

module.exports = {
    feed, 
}
