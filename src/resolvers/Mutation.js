
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../utils')

async function signup(parent, args, context, info){
    const password = await bcrypt.hash(args.password, 10)
    const user = await context.prisma.createUser({ ...args, password})
    const token = jwt.sign({userId: user.id}, APP_SECRET)

    return {
        token,
        user,
    }
}


async function login(parent, args, context, info){
    const user = await context.prisma.user({email: args.email})
    if(!user) {
        throw new Error("User unavailable")
    }
    const valid = await bcrypt.compare(args.password, user.password)
    if(!valid){
        throw new Error("Invalid Password Please check password")
    }
    const token = jwt.sign({ userId: user.id }, APP_SECRET)

    return{
        token,
        user
    }
}

function post (parent, args, context, info){
    const userId = getUserId(context)
    return context.prisma.createLink({
      url: args.url, 
      description: args.description,
      postedBy: { connect: { id: userId }},
    })
}

/**
 * In this the user ID is validated and checked
 * whether the valid user has already voted for the link
 * and if the user has voted error will be thrown
 * if else a vote will be created
 */
async function vote(parent, args, context, info){
    //validating the user with the jwt token
    const userId = getUserId(context)

    //$exists is like a where clause used to filter a item 
    const voteExists = await context.prisma.$exists.vote({
        user: { id: userId },
        link: { id: args.linkId }
    })

    //throw error if user voted
    if(voteExists){
        throw new Error(`Already voted for link ${args.linkId}`)
    }

    return context.prisma.createVote({
        user: {connect: {id: userId}},
        link: {connect: {id: args.linkId}},
    })
}


module.exports = {
    signup,
    login,
    post,
    vote,
}