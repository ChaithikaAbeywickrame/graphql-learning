/**
 * Example 02
 */
let links = [{
    id: 'link-0',
    url: 'www.howtographql.com',
    description: 'Fullstack tutorial for GraphQL'
},{
    id: 'link-1',
    url: 'www.howtographql.com',
    description: 'Fullstack tutorial for GraphQL 1'
},{
    id: 'link-2',
    url: 'www.howtographql.com',
    description: 'Fullstack tutorial for GraphQL 2'
}]

let idCount = links.length
const resolvers = { 
    Query:{
        info: () => `this is the sample information`,
        feed: () => links,
        getLink: (parent, args) => {
          return links.find(link => link.id === args.id);
        }
    },
    Mutation: {
      post: (parent, args) => {
        const link = {
          id: `Link-${idCount++}`,
          description: args.description,
          url: args.url,
        }
        links.push(link)
        return link;
      },

      updateLink: (parent, args) => {
        linkIndex = links.findIndex(link => link.id === args.id)
        console.log(linkIndex)
        if(linkIndex != -1){
          links[linkIndex].description = args.description
          links[linkIndex].url = args.url
          return links[linkIndex] 
        }else{
          console.log("Link unavailable")
        }
      }
    },
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
})

server.start(() => console.log(`Server is running on http://localhost:4000`))